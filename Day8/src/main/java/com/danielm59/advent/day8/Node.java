package com.danielm59.advent.day8;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static com.danielm59.advent.day8.Day8.data;
import static com.danielm59.advent.day8.Day8.position;
import static com.danielm59.advent.day8.Day8.totalMeta;

public class Node
{
	private int childrenCount;
	private int metaCount;

	private LinkedHashMap<Node, Integer> childen = new LinkedHashMap<>();
	private ArrayList<Integer> meta = new ArrayList<>();


	public Node(int children, int metaCount)
	{
		this.childrenCount = children;
		this.metaCount = metaCount;
	}

	public int process()
	{
		for (int i = 0; i < childrenCount; i++)
		{
			Node child = new Node(data.get(position++), data.get(position++));
			int value = child.process();
			childen.put(child, value);
		}
		for (int i = 0; i < metaCount; i++)
		{
			int metaData = data.get(position++);
			totalMeta += metaData;
			meta.add(metaData);
		}
		if (childrenCount == 0)
		{
			int sum = 0;
			for (int metaValue : meta)
			{
				sum += metaValue;
			}
			return sum;
		} else
		{
			int sum = 0;
			for (int metaValue : meta)
			{
				if (metaValue > 0 && metaValue <= childen.size())
				{
					sum += (new ArrayList<>(childen.values())).get(metaValue - 1);
				}
			}
			return sum;
		}
	}
}
