package com.danielm59.advent.day8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class Day8
{
	public static ArrayList<Integer> data;
	public static int position = 0;
	public static int totalMeta = 0;

	public static void main(String[] args)
	{
		data = loadData("input.txt");
		int part2 = run(data);
		System.out.println(String.format("Part 1: %d", totalMeta));
		System.out.println(String.format("Part 2: %d", part2));
	}

	private static ArrayList<Integer> loadData(String fileName)
	{
		try
		{
			ArrayList<Integer> data = new ArrayList<>();
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				String[] lineArray = line.split(" ");
				for (String inputValue : lineArray)
				{
					data.add(Integer.parseInt(inputValue));
				}
			}
			return data;

		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static int run(ArrayList<Integer> data)
	{
		int value = 0;
		while (position < data.size())
		{
			value += new Node(data.get(position++), data.get(position++)).process();
		}
		return value;
	}
}
