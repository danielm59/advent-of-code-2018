package com.danielm59.advent.day12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day12
{
	private static String initalState;
	public static HashMap<String, Character> data;
	private static int fillerSize = 25;

	public static void main(String[] args)
	{
		data = loadData("input.txt");
		char[] chars = new char[fillerSize];
		Arrays.fill(chars, '.');
		String filler = new String(chars);
		initalState = filler + initalState + filler;
		System.out.println(String.format("Part 1: %d", run(20)));
	}

	private static HashMap<String, Character> loadData(String fileName)
	{
		try
		{
			Pattern patternHeader = Pattern.compile("initial state: (.*)");
			Pattern pattern = Pattern.compile("(.*) => (.*)");
			HashMap<String, Character> data = new HashMap<>();

			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				Matcher matcherHeader = patternHeader.matcher(line);
				if (matcherHeader.matches())
				{
					initalState = matcherHeader.group(1);
				}
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches())
				{
					data.put(matcher.group(1), matcher.group(2).charAt(0));
				}
			}
			return data;
		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static int run(int steps)
	{
		String lastState = initalState;
		for (int i = 0; i < steps; i++)
		{
			StringBuilder currentState = new StringBuilder();
			for (int c = 0; c < lastState.length(); c++)
			{
				if (c < 2 || c > lastState.length() - 3)
				{
					currentState.append(lastState.charAt(c));
				} else
				{
					String pattern = lastState.substring(c - 2, c + 3);
					if (data.containsKey(pattern))
					{
						currentState.append(data.get(pattern));
					} else
					{
						currentState.append(lastState.charAt(c));
					}
				}
			}
			lastState = currentState.toString();
			System.out.println(String.format("Step %02d: %s", i + 1, lastState));
		}
		int total = 0;
		for (int c = 0; c < lastState.length(); c++)
		{
			if (lastState.charAt(c) == '#')
			{
				total += c - fillerSize;
			}
		}
		return total;
	}
}
