package com.danielm59.advent.day1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;

public class Day1
{
	public static void main(String[] args)
	{
		ArrayList<Integer> data = loadData("input.txt");
		System.out.println(String.format("Part 1: %d", part1(data)));
		System.out.println(String.format("Part 2: %d", part2(data)));
	}

	private static ArrayList<Integer> loadData(String fileName)
	{
		try
		{
			ArrayList<Integer> data = new ArrayList<>();
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				data.add(Integer.parseInt(line));
			}
			return data;

		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static int part1(ArrayList<Integer> data)
	{
		int total = 0;
		for (int value : data)
		{
			total += value;
		}
		return total;
	}

	private static int part2(ArrayList<Integer> data)
	{
		int total = 0;
		HashSet<Integer> seen = new HashSet<>();
		while (true)
		{
			for (int value : data)
			{
				total += value;
				if (seen.contains(total))
				{
					return total;
				} else
				{
					seen.add(total);
				}
			}
		}
	}
}
