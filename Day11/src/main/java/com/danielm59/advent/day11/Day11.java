package com.danielm59.advent.day11;

public class Day11
{
	public static final int serialNumber = 9221;

	public static void main(String[] args)
	{
		int[] ans = run();
		System.out.println(String.format("Answer : %d,%d,%d", ans[0], ans[1], ans[2]));
	}

	private static int[] run()
	{
		int[][] powerLevels = new int[301][301];
		for (int x = 1; x <= 300; x++)
		{
			for (int y = 1; y <= 300; y++)
			{
				int rackID = x + 10;            //Find the fuel cell's rack ID, which is its X coordinate plus 10.
				int powerLevel = rackID * y;    //Begin with a power level of the rack ID times the Y coordinate.
				powerLevel += serialNumber;     //Increase the power level by the value of the grid serial number
				powerLevel *= rackID;           //Set the power level to itself multiplied by the rack ID.
				powerLevel /= 100;              //Keep only the hundreds digit of the power level
				powerLevel %= 10;
				powerLevel -= 5;                //Subtract 5 from the power level.
				powerLevels[x][y] = powerLevel;
			}
		}
		int maxPower = 0;
		int[] maxPos = new int[3];

		for (int size = 1; size <=300; size++) //set size to only 3 for part 1
		//int size = 3;
		{
			System.out.println("size: " + size);
			for (int x = 1; x < 300 - size; x++)
			{
				for (int y = 1; y < 300 - size; y++)
				{
					int power = 0;
					for (int i = 0; i < size; i++)
					{
						for (int j = 0; j < size; j++)
						{
							power += powerLevels[x + i][y + j];
						}
					}
					if (power > maxPower)
					{
						maxPower = power;
						maxPos = new int[]{x, y, size};
					}
				}
			}
		}
		return maxPos;
	}
}
