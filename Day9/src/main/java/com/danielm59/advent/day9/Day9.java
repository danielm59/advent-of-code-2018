package com.danielm59.advent.day9;

import java.util.ArrayDeque;
import java.util.Arrays;

public class Day9
{
		public static int players = 459;
	public static int lastMarble = 71320;

	public static void main(String[] args)
	{
		System.out.println(String.format("Part 1: %d", run()));
		lastMarble *= 100;
		System.out.println(String.format("Part 2: %d", run()));
	}


	private static long run()
	{
		long[] scores = new long[players];

		CircleDeque<Integer> circle = new CircleDeque<>();
		circle.addFirst(0);
		for (int i = 1; i <= lastMarble; i++)
		{
			if (i % 23 == 0) {
				circle.rotate(-7);
				scores[i % players] += i + circle.pop();

			} else {
				circle.rotate(2);
				circle.addLast(i);
			}
		}
		return Arrays.stream(scores).max().getAsLong();
	}

	static class CircleDeque<T> extends ArrayDeque<T>
	{
		void rotate(int num)
		{
			if (num == 0) return;
			if (num > 0)
			{
				for (int i = 0; i < num; i++)
				{
					T t = this.removeLast();
					this.addFirst(t);
				}
			} else
			{
				for (int i = 0; i < Math.abs(num) - 1; i++)
				{
					T t = this.remove();
					this.addLast(t);
				}
			}

		}
	}
}
