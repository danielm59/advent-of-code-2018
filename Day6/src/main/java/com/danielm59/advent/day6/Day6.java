package com.danielm59.advent.day6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day6
{
	public static void main(String[] args)
	{
		ArrayList<Area> data = loadData("input.txt");
		System.out.println(String.format("Part 1: %d", part1(data)));
		System.out.println(String.format("Part 2: %d", part2(data)));
	}

	private static ArrayList<Area> loadData(String fileName)
	{
		try
		{
			Pattern pattern = Pattern.compile("(\\d+), (\\d+)");
			ArrayList<Area> data = new ArrayList<>();
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches())
				{
					data.add(new Area(matcher.group(1), matcher.group(2)));
				}
			}
			return data;
		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static int part1(ArrayList<Area> data)
	{
		for (int x = 0; x < 1000; x++)
		{
			for (int y = 0; y < 1000; y++)
			{
				int shortestDistance = Integer.MAX_VALUE;
				Area nearest = null;
				int nearestCount = 0;

				for (Area area : data)
				{
					int dist = Math.abs(area.getPosX() - x) + Math.abs(area.getPosY() - y);
					if (dist < shortestDistance)
					{
						shortestDistance = dist;
						nearest = area;
						nearestCount = 1;
					} else if (dist == shortestDistance)
					{
						nearestCount++;
					}
				}
				if (nearestCount == 1)
				{
					nearest.incrementSize();
					if (x == 0 || x == 999 || y == 0 || y == 999)
					{
						nearest.setInfinite();
					}
				}
			}
		}
		Area largest = null;

		for (Area area : data)
		{
			if (!area.isInfinite())
			{
				if (largest == null || area.getSize() > largest.getSize())
				{
					largest = area;
				}
			}
		}
		return largest.getSize();
	}

	private static int part2(ArrayList<Area> data)
	{
		int totalArea = 0;
		for (int x = 0; x < 1000; x++)
		{
			for (int y = 0; y < 1000; y++)
			{
				int dist = 0;
				for (Area area : data)
				{
					dist += Math.abs(area.getPosX() - x) + Math.abs(area.getPosY() - y);
				}
				if (dist < 10000)
				{
					totalArea++;
				}
			}
		}
		return totalArea;
	}
}
