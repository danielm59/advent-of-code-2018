package com.danielm59.advent.day6;

public class Area
{
	private int posX;
	private int posY;
	private int size = 0;
	private boolean infinite = false;

	public Area(String x, String y)
	{
		posX = Integer.parseInt(x);
		posY = Integer.parseInt(y);
	}

	public int getPosX()
	{
		return posX;
	}

	public int getPosY()
	{
		return posY;
	}

	public int getSize()
	{
		return size;
	}

	public void incrementSize()
	{
		size++;
	}

	public boolean isInfinite()
	{
		return infinite;
	}

	public void setInfinite()
	{
		infinite = true;
	}
}
