package com.danielm59.advent.day5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Day5
{
	public static void main(String[] args)
	{
		String data = loadData("input.txt");
		System.out.println(String.format("Part 1: %d", part1(data)));
		System.out.println(String.format("Part 2: %d", part2(data)));
	}

	private static String loadData(String fileName)
	{
		try
		{
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			if ((line = br.readLine()) != null) return line; //Data is a single line in this task

		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static int part1(String data)
	{
		int oldLength = Integer.MAX_VALUE;

		while (oldLength > data.length())
		{
			oldLength = data.length();
			for (int i = 0; i < 26; i++)
			{
				char char1 = (char) ('a' + i);
				char char2 = (char) ('A' + i);
				data = data.replaceAll("" + char1 + char2, "");
				data = data.replaceAll("" + char2 + char1, "");
			}
		}

		return data.length();
	}

	private static int part2(String data)
	{
		int shortest = Integer.MAX_VALUE;
		for (int i = 0; i < 26; i++)
		{
			String tempData = data;
			char char1 = (char) ('a' + i);
			char char2 = (char) ('A' + i);
			tempData = tempData.replaceAll("" + char1, "");
			tempData = tempData.replaceAll("" + char2, "");
			int length = part1(tempData);
			if (length<shortest) shortest = length;
		}
		return shortest;
	}
}
