package com.danielm59.advent.day4;

import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day4
{
	public static void main(String[] args)
	{
		ArrayList<Event> data = loadData("input.txt");
		data.sort(Comparator.comparing(Event::getTime));
		System.out.println(String.format("Part 1: %d", part1(data)));
		System.out.println(String.format("Part 2: %d", part2(data)));
	}

	private static ArrayList<Event> loadData(String fileName)
	{
		try
		{
			Pattern pattern = Pattern.compile("\\[(.*)] (.*)");
			ArrayList<Event> data = new ArrayList<>();
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches())
				{
					data.add(new Event(matcher.group(1), matcher.group(2)));
				}
			}
			return data;
		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static long part1(ArrayList<Event> data)
	{
		int currentGuard = 0;
		DateTime sleepTime = null;
		HashMap<Integer, Long> sleepAmounts = new HashMap<>();
		for (Event event : data)
		{
			switch (event.getEventType())
			{
				case START:
					currentGuard = event.getGuardID();
					break;
				case SLEEP:
					sleepTime = event.getTime();
					break;
				case WAKE:
					long timeSlept = TimeUnit.MILLISECONDS.toMinutes((event.getTime().getMillis() - sleepTime.getMillis()));
					if (sleepAmounts.containsKey(currentGuard))
					{
						sleepAmounts.put(currentGuard, sleepAmounts.get(currentGuard) + timeSlept);
					} else
					{
						sleepAmounts.put(currentGuard, timeSlept);
					}
					break;
			}
		}
		Map.Entry<Integer, Long> maxEntry = null;

		for (Map.Entry<Integer, Long> entry : sleepAmounts.entrySet())
		{
			if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
			{
				maxEntry = entry;
			}
		}

		int sleepyGuard = maxEntry.getKey();

		HashMap<Integer, Integer> sleeps = new HashMap<>();
		for (Event event : data)
		{
			switch (event.getEventType())
			{
				case START:
					currentGuard = event.getGuardID();
					break;
				case SLEEP:
					if (currentGuard == sleepyGuard)
					{
						sleepTime = event.getTime();
					}
					break;
				case WAKE:
					if (currentGuard == sleepyGuard)
					{
						while (sleepTime.getMinuteOfHour() < event.getTime().getMinuteOfHour())
						{
							if (sleeps.containsKey(sleepTime.getMinuteOfHour()))
							{
								sleeps.put(sleepTime.getMinuteOfHour(), sleeps.get(sleepTime.getMinuteOfHour()) + 1);
							} else
							{
								sleeps.put(sleepTime.getMinuteOfHour(), 1);
							}
							sleepTime = sleepTime.plusMinutes(1);
						}
					}
					break;
			}
		}

		Map.Entry<Integer, Integer> maxEntry2 = null;

		for (Map.Entry<Integer, Integer> entry : sleeps.entrySet())
		{
			if (maxEntry2 == null || entry.getValue().compareTo(maxEntry2.getValue()) > 0)
			{
				maxEntry2 = entry;
			}
		}

		return sleepyGuard * (maxEntry2.getKey());
	}

	private static int part2(ArrayList<Event> data)
	{
		int currentGuard = 0;
		DateTime sleepTime = null;
		HashMap<Integer, HashMap<Integer, Integer>> sleeps = new HashMap<>();
		for (Event event : data)
		{
			switch (event.getEventType())
			{
				case START:
					currentGuard = event.getGuardID();
					break;
				case SLEEP:
					sleepTime = event.getTime();
					break;
				case WAKE:

					HashMap<Integer, Integer> guardSleeps;
					if (sleeps.containsKey(currentGuard))
					{
						guardSleeps = sleeps.get(currentGuard);
					} else
					{
						guardSleeps = new HashMap<>();
					}

					while (sleepTime.getMinuteOfHour() < event.getTime().getMinuteOfHour())
					{

						if (guardSleeps.containsKey(sleepTime.getMinuteOfHour()))
						{
							guardSleeps.put(sleepTime.getMinuteOfHour(), guardSleeps.get(sleepTime.getMinuteOfHour()) + 1);
						} else
						{
							guardSleeps.put(sleepTime.getMinuteOfHour(), 1);
						}
						sleepTime = sleepTime.plusMinutes(1);
					}

					sleeps.put(currentGuard, guardSleeps);
			}
		}

		int maxGuard = 0;
		Map.Entry<Integer, Integer> maxEntry = null;

		for (Map.Entry<Integer, HashMap<Integer, Integer>> guardEntry : sleeps.entrySet())
		{
			for (Map.Entry<Integer, Integer> entry : guardEntry.getValue().entrySet())
			{
				if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
				{
					maxGuard = guardEntry.getKey();
					maxEntry = entry;
				}
			}
		}

		return maxGuard * maxEntry.getKey();
	}
}




