package com.danielm59.advent.day4;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Event
{
	static Pattern pattern = Pattern.compile("Guard #(\\d+) begins shift");

	private DateTime time;
	private EventType eventType;
	private String event;
	private int guardID = 0;

	public Event(String time, String event)
	{
		time = time.replace("1518", "2018");
		this.time = DateTime.parse(time, DateTimeFormat.forPattern("yyyy-MM-dd HH:mm"));
		this.event = event;
		switch (event)
		{
			case "falls asleep":
				eventType = EventType.SLEEP;
				break;
			case "wakes up":
				eventType = EventType.WAKE;
				break;
			default:
				eventType = EventType.START;
				Matcher m = pattern.matcher(event);
				if (m.matches())
				{
					guardID = Integer.parseInt(m.group(1));
				} else
				{
					System.out.println(event);
				}
				break;
		}
	}

	public DateTime getTime()
	{
		return time;
	}

	public EventType getEventType()
	{
		return eventType;
	}

	public String getEvent()
	{
		return event;
	}

	public int getGuardID()
	{
		return guardID;
	}

	public enum EventType
	{
		START, SLEEP, WAKE
	}
}
