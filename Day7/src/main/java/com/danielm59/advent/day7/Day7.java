package com.danielm59.advent.day7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day7
{
	public static HashMap<String, Task> data;

	public static void main(String[] args)
	{
		data = loadData("input.txt");
		System.out.println(String.format("Part 1: %s", part1()));
		data = loadData("input.txt"); //reload data for part 2 as we need to reset the complete status
		System.out.println(String.format("Part 2: %d", part2()));
	}

	private static HashMap<String, Task> loadData(String fileName)
	{
		try
		{
			Pattern pattern = Pattern.compile("Step (\\w) must be finished before step (\\w) can begin.");
			HashMap<String, Task> data = new HashMap<>();

			for (int i = 0; i < 26; i++)
			{
				String taskName = String.valueOf((char) ('A' + i));
				data.put(taskName, new Task(61 + i));
			}

			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches())
				{
					data.get(matcher.group(2)).addDependency(data.get(matcher.group(1)));
				}
			}
			return data;
		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static String part1()
	{
		StringBuilder order = new StringBuilder();
		while (order.length() < 26)
		{
			for (Map.Entry<String, Task> task : data.entrySet())
			{
				if (!task.getValue().isCompleate() && task.getValue().canRun())
				{
					order.append(task.getKey());
					task.getValue().setCompleate();
					break; //break out of for loop to make sure tasks are prioritised in alphabetical order
				}
			}
		}
		return order.toString();
	}

	private static int part2()
	{
		int time = 0;

		Worker[] workers = {new Worker(1), new Worker(2), new Worker(3), new Worker(4), new Worker(5)};

		boolean busy = true;
		while (busy)
		{
			time++;
			busy = false;
			for (Worker worker : workers)
			{
				worker.tick(time);
				busy |= worker.isBusy();
			}
		}

		return time - 1; //remove last tick as it was used to remove the completed tasks
	}
}
