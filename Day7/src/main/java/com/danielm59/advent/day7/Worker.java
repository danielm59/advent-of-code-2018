package com.danielm59.advent.day7;

import java.util.Map;

import static com.danielm59.advent.day7.Day7.data;

public class Worker
{
	private int workerID;
	private Task currentTask = null;
	private String taskName = "";

	public Worker(int i)
	{
		workerID = i;
	}

	public void tick(int time)
	{
		//remove task that finished last tick
		if (currentTask != null)
		{
			if (currentTask.getTaskLength() == 0)
			{
				System.out.println(String.format("%d: Task %s finished by worker %d", time - 1, taskName, workerID));
				currentTask.setCompleate();
				currentTask = null;
			}
		}

		//get new task
		if (currentTask == null)
		{
			for (Map.Entry<String, Task> taskInfo : data.entrySet())
			{
				Task task = taskInfo.getValue();
				if (!task.isInProgress() && task.canRun())
				{
					taskName = taskInfo.getKey();
					System.out.println(String.format("%d: Task %s started by worker %d", time, taskName, workerID));
					task.setInProgress();
					currentTask = task;
					break; //task found
				}
			}
		}
		//tick task
		if (currentTask != null)
		{
			currentTask.tick();
		}
	}

	boolean isBusy()
	{
		return (currentTask != null);
	}
}
