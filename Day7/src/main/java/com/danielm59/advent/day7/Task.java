package com.danielm59.advent.day7;

import java.util.ArrayList;

public class Task
{

	private ArrayList<Task> dependencies = new ArrayList<>();
	private boolean inProgress = false;
	private boolean isCompleate = false;
	private int taskLength;

	public Task(int length)
	{
		taskLength = length;
	}

	public void addDependency(Task dependency)
	{
		dependencies.add(dependency);
	}

	public boolean isCompleate()
	{
		return isCompleate;
	}

	public void setCompleate()
	{
		isCompleate = true;
	}

	public boolean canRun()
	{
		for (Task dependency : dependencies)
		{
			if (!dependency.isCompleate)
			{
				return false;
			}
		}
		return true;
	}

	public int getTaskLength()
	{
		return taskLength;
	}

	public void tick()
	{
		taskLength--;
	}

	public void setInProgress()
	{
		inProgress = true;
	}

	public boolean isInProgress()
	{
		return inProgress;
	}
}

