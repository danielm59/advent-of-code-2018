package com.danielm59.advent.day2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Day2
{
	public static void main(String[] args)
	{
		ArrayList<String> data = loadData("input.txt");
		System.out.println(String.format("Part 1: %d", part1(data)));
		System.out.println(String.format("Part 2: %s", part2(data)));
	}

	private static ArrayList<String> loadData(String fileName)
	{
		try
		{
			ArrayList<String> data = new ArrayList<>();
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				data.add(line);
			}
			return data;

		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static int part1(ArrayList<String> data)
	{
		int twos = 0;
		int threes = 0;
		for (String line : data)
		{
			boolean two = false;
			boolean three = false;
			HashMap<Character, Integer> counters = new HashMap<>();
			//count chars
			for (char c : line.toCharArray())
			{
				if (counters.containsKey(c))
				{
					counters.put(c, counters.get(c) + 1);
				} else
				{
					counters.put(c, 1);
				}
			}
			//check counts
			for (Map.Entry<Character, Integer> entry : counters.entrySet())
			{
				if (entry.getValue() == 2) two = true;
				if (entry.getValue() == 3) three = true;
			}
			if (two) twos++;
			if (three) threes++;
		}
		//return checksum
		return twos * threes;
	}

	private static String part2(ArrayList<String> data)
	{
		String[] dataArray = data.toArray(new String[0]);
		for (int i = 0; i < dataArray.length - 1; i++)
		{
			for (int j = i + 1; j < dataArray.length; j++)
			{
				String s1 = dataArray[i];
				String s2 = dataArray[j];
				int idx;
				if ((idx = checkStrings(s1, s2)) > 0)
				{
					return s1.substring(0, idx) + s1.substring(idx + 1);
				}
			}
		}
		return "";
	}

	private static int checkStrings(String s1, String s2)
	{
		int diff = -1;
		for (int k = 0; k < s1.length(); k++)
		{
			if (s1.charAt(k) != s2.charAt(k))
			{
				if (diff < 0) diff = k;
				else return -1;
			}
		}
		return diff;
	}

}
