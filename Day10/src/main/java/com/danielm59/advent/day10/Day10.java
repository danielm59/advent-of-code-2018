package com.danielm59.advent.day10;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day10
{
	static ArrayList<Entity> data;
	static int time = -1;
	static int end = 10227;//found by watching and slowing down the animation

	static final int windowSize = 900;

	static int translate = 0;
	static float scale = 1;

	public static void main(String[] args)
	{
		data = loadData("input.txt");

		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("Day 10");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBackground(Color.white);
		frame.setSize(windowSize, windowSize);

		Panel panel = new Panel(data);

		frame.add(panel);

		frame.setVisible(true);
	}

	private static ArrayList<Entity> loadData(String fileName)
	{
		try
		{
			Pattern pattern = Pattern.compile("position=<\\s*(.*),\\s*(.*)> velocity=<\\s*(.*),\\s*(.*)>");
			ArrayList<Entity> data = new ArrayList<>();
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches())
				{
					data.add(new Entity(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4)));
				}
			}
			return data;
		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	public static void update()
	{
		time+=1;
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for (Entity e : data)
		{
			int[] pos = e.getPos(time);
			if (pos[0] < min) min = pos[0];
			if (pos[0] > max) max = pos[0];
			if (pos[1] < min) min = pos[1];
			if (pos[1] > max) max = pos[1];
		}

		translate = min;
		scale = (max - min) / windowSize;
		if(scale < 1) scale = 1;
	}
}
