package com.danielm59.advent.day10;

public class Entity
{
	private int posX;
	private int posY;
	private int velX;
	private int velY;

	public Entity(String posX, String posY, String velX, String velY)
	{
		this.posX = Integer.parseInt(posX);
		this.posY = Integer.parseInt(posY);
		this.velX = Integer.parseInt(velX);
		this.velY = Integer.parseInt(velY);
	}

	public int[] getPos(int time)
	{
		return new int[]{posX + velX * time, posY + velY * time};
	}
}
