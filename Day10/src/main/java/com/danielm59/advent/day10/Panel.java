package com.danielm59.advent.day10;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

import static com.danielm59.advent.day10.Day10.end;
import static com.danielm59.advent.day10.Day10.scale;
import static com.danielm59.advent.day10.Day10.time;
import static com.danielm59.advent.day10.Day10.translate;

public class Panel extends JPanel
{
	private ArrayList<Entity> data;

	public Panel(ArrayList<Entity> data)
	{
		this.data = data;
	}

	public void paintComponent(Graphics g)
	{
		if (time < end) Day10.update();
		super.paintComponent(g);
		g.drawString(String.valueOf(time), 25, 25);
		for (Entity e : data)
		{
			int[] pos = e.getPos(time);
			g.fillOval((int) ((pos[0] - translate) / scale), (int) ((pos[1] - translate) / scale), 3, 3);
		}
		repaint();
	}
}
