package com.danielm59.advent.day3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day3
{
	public static void main(String[] args)
	{
		ArrayList<Claim> data = loadData("input.txt");
		System.out.println(String.format("Part 1: %d", part1(data)));
		System.out.println(String.format("Part 2: %s", part2(data)));
	}

	private static ArrayList<Claim> loadData(String fileName)
	{
		try
		{
			Pattern pattern = Pattern.compile("#\\d+ @ (\\d+),(\\d+): (\\d+)x(\\d+)");

			ArrayList<Claim> data = new ArrayList<>();
			File input = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(input));
			String line;
			while ((line = br.readLine()) != null)
			{
				Matcher matcher = pattern.matcher(line);
				if (matcher.matches())
				{
					data.add(new Claim(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4)));
				}
			}
			return data;

		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	private static int part1(ArrayList<Claim> data)
	{
		int[][] fabric = new int[1000][1000];

		for (Claim claim : data)
		{
			for (int i = claim.posX(); i < claim.posX() + claim.sizeX(); i++)
			{
				for (int j = claim.posY(); j < claim.posY() + claim.sizeY(); j++)
				{
					fabric[i][j]++;
				}
			}
		}
		int count = 0;
		for (int[] row : fabric)
		{
			for (int pos : row)
			{
				if (pos > 1)
					count++;
			}
		}
		return count;
	}

	private static int part2(ArrayList<Claim> data)
	{
		Claim[] dataArray = data.toArray(new Claim[0]);
		for (int i = 0; i < dataArray.length; i++)
		{
			for (int j = i + 1; j < dataArray.length; j++)
			{
				if (dataArray[i].isOverlapped() && dataArray[j].isOverlapped()) continue;
				if (checkOverlapped(dataArray[i],dataArray[j]))
				{
					dataArray[i].setOverlapped();
					dataArray[j].setOverlapped();
				}
			}
			if (!dataArray[i].isOverlapped()) return i + 1;
		}
		return 0;
	}

	private static boolean checkOverlapped(Claim claim1, Claim claim2)
	{
		if (claim1.posX() > claim2.posX() + claim2.sizeX() || claim2.posX() > claim1.posX() + claim1.sizeX())
			return false;

		if (claim1.posY() > claim2.posY() + claim2.sizeY() || claim2.posY() > claim1.posY() + claim1.sizeY())
			return false;

		return true;
	}
}
