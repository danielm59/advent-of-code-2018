package com.danielm59.advent.day3;

public class Claim
{
	private int posX;
	private int posY;
	private int sizeX;
	private int sizeY;

	private boolean overlapped = false;

	public Claim(String posX, String posY, String sizeX, String sizeY)
	{
		this.posX = Integer.parseInt(posX);
		this.posY = Integer.parseInt(posY);
		this.sizeX = Integer.parseInt(sizeX);
		this.sizeY = Integer.parseInt(sizeY);
	}

	public int posX()
	{
		return posX;
	}

	public int posY()
	{
		return posY;
	}

	public int sizeX()
	{
		return sizeX;
	}

	public int sizeY()
	{
		return sizeY;
	}

	public boolean isOverlapped()
	{
		return overlapped;
	}

	public void setOverlapped()
	{
		this.overlapped = true;
	}
}
